var loader = new THREE.FontLoader();

loader.load( 'https://code.weebo.fr/projgl/Programmes/jedi.json', function ( font )
{
	var geometry = new THREE.TextGeometry( 'Hello three.js!', {
		font: font,
		size: 80,
		height: 5,
		curveSegments: 12,
		bevelEnabled: true,
		bevelThickness: 10,
		bevelSize: 8,
		bevelOffset: 0,
		bevelSegments: 5
    });
    console.log(geometry);
});