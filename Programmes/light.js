function InitLight()
{
    var ambientlight = new THREE.AmbientLight( 0xffffff ,0.015 );
    scene.add( ambientlight );
                
    {
        //Mise en place torches

        lampeTorche = new THREE.Mesh(new THREE.CylinderGeometry(1, 1, 7, 20), new THREE.MeshPhongMaterial({color:0x000000}));
        lampeTorche.rotateX(Math.PI/2);
        lampeTorche.name="lampetorche";

        camera.add(lampeTorche);

        var intensite = 0.5;
        var distance = 150;
        var power = 4000;
        var angle =0.2;
        
        spotLight = new THREE.SpotLight(0xffffff, intensite, distance);
        spotLight.power = power;
        spotLight.angle = angle;
        spotLight.decay = 2.5;
        spotLight.penumbra = 0.1;
        spotLight.distance = 200;
        spotLight.castShadow = true;
        spotLight.rotateX(Math.PI/2);

        lampeTorche.add(spotLight);
        lampeTorche.add(spotLight.target);

        spotLight2 = new THREE.SpotLight(0xCCCCCC, intensite, distance);
        spotLight2.power = power/2;
        spotLight2.angle = angle+0.05;
        spotLight2.decay = 2.5;
        spotLight2.penumbra = 0.1;
        spotLight2.distance = 200;
        spotLight2.castShadow = true;
        spotLight2.rotateX(Math.PI/2);

        lampeTorche.add(spotLight2);
        lampeTorche.add(spotLight2.target);

        
        spotLight3 = new THREE.SpotLight(0xFFFFFF, intensite, distance);
        spotLight3.power = power;
        spotLight3.angle = angle+0.1;
        spotLight3.decay = 3;
        spotLight3.penumbra = 0.1;
        spotLight3.distance = 200;
        spotLight3.castShadow = true;
        spotLight3.rotateX(Math.PI/2);

        lampeTorche.add(spotLight3);
        lampeTorche.add(spotLight3.target);
    }
}